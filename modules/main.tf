provider "aws" {
    access_key = "${var.aws_access_key}"
    secret_key = "${var.aws_secret_key}"
    region = "${var.region}"
}


module "s3"{
    source = "/home/jason/project1/modules/s3"
    bucket_name = "test-bucket-one-jasonshi"
}